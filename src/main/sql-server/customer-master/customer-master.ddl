CREATE TABLE "dbo"."CustomerInfo"  ( 
	"CustomerInfoSk"	int NOT NULL,
	"PartySk"       	int NOT NULL,
	"SSN"           	varchar(25) NOT NULL,
	"Phone Number"  	varchar(25) NULL,
	"First Name"    	varchar(256) NOT NULL,
	"Last Name"     	varchar(256) NOT NULL,
	"Middle Name"   	varchar(256) NOT NULL,
	"Address Line 1"	varchar(256) NOT NULL,
	"Address Line 2"	varchar(256) NULL,
	"City"          	varchar(256) NOT NULL,
	"State"         	varchar(256) NOT NULL,
	"Zip"           	varchar(12) NOT NULL,
	CONSTRAINT "PK_CustomerInfo" PRIMARY KEY CLUSTERED("CustomerInfoSk")
)
;;

CREATE TABLE "dbo"."Party"  ( 
	"PartySk"    	int NOT NULL,
	"PartyTypeSk"	int NOT NULL,
	"PartyName"  	varchar(256) NOT NULL,
	CONSTRAINT "PK_Party" PRIMARY KEY CLUSTERED("PartySk")
)
;;

CREATE TABLE "dbo"."PartyType"  ( 
	"PartyTypeSk"  	int NOT NULL,
	"PartyTypeName"	varchar(256) NOT NULL,
	CONSTRAINT "PK_PartyType" PRIMARY KEY CLUSTERED("PartyTypeSk")
)
;;

ALTER TABLE "dbo"."CustomerInfo"
	ADD CONSTRAINT "FK_CustomerInfo_Party"
	FOREIGN KEY("PartySk")
	REFERENCES "dbo"."Party"("PartySk")
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
;;
ALTER TABLE "dbo"."Party"
	ADD CONSTRAINT "FK_Party_PartyType"
	FOREIGN KEY("PartyTypeSk")
	REFERENCES "dbo"."PartyType"("PartyTypeSk")
	ON DELETE NO ACTION 
	ON UPDATE NO ACTION 
;;

CREATE SCHEMA VW
;;

CREATE VIEW "VW"."VwCustomerGl"
AS
SELECT
    CI.CustomerInfoSk,
    (CI.SSN % 1000) AS SSM,
    PT.PartyTypeName,
    CI.SSN,
    CI."Phone Number",
    CI."First Name",
    CI."Middle Name",
    CI."Last Name",
    CI."Address Line 1",
    CI.City,
    CI.State,
    CI.Zip
FROM
    CustomerInfo CI
JOIN
    Party P
        ON
            CI.PartySk = P.PartySk
JOIN
    PartyType PT
        ON
            P.PartyTypeSk = PT.PartyTypeSk
;;